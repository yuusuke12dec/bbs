# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  topic_id   :integer          not null
#  body       :text(65535)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_comments_on_topic_id  (topic_id)
#

class Comment < ApplicationRecord
  belongs_to :topic

  validates :body, presence: true
end

# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_topics_on_name  (name) UNIQUE
#

class Topic < ApplicationRecord
  has_many :comments

  validates :name, presence: true
  validates :name, allow_nil: true, uniqueness: true
end

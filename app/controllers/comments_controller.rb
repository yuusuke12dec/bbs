class CommentsController < ApplicationController
  def create
    @topic = Topic.find(params[:topic_id])
    @comment = Comment.new(comment_params.merge(topic: @topic))
    if @comment.save
      redirect_to "/topics/#{@topic.id}"
    else
      @validation_errors = @topic.errors.full_messages
      @comment = Comment.new
      render 'topics/show'
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end
end

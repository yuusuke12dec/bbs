class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # better_errors hack
  # https://github.com/charliesome/better_errors/issues/341
  before_action -> { request.env['puma.config'].options.user_options.delete(:app) if request.env.key?('puma.config') }, if: -> { Rails.env.development? }
end

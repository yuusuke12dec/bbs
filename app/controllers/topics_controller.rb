class TopicsController < ApplicationController
  def index
    @topics = Topic.all
    @topic = Topic.new
  end

  def show
    @topic = Topic.find(params[:id])
    @comment = Comment.new
  end

  def create
    @topic = Topic.new(topic_params)
    if @topic.save
      redirect_to "/topics/#{@topic.id}"
    else
      @validation_errors = @topic.errors.full_messages
      @topics = Topic.all
      render :index
    end
  end

  private

  def topic_params
    params.require(:topic).permit(:name)
  end
end

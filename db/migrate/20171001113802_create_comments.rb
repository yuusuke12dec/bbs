class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.integer :topic_id, null: false, index: true
      t.text :body, null: false
      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  root 'topics#index'
  resources :topics, only: [:show, :create], shallow: true do
    resources :comments, only: [:create]
  end
end
